
#---------------------------------------------------------------------------

#!/bin/bash

# Script pour préparer l'environnement de déploiement #
sudo apt-get update 
# Installation de nginx (web servire) et Démarrage du service
sudo apt-get install nginx
sudo systemctl start nginx
# Installation de node 
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt update
sudo apt install nodejs
# Installation de npm et pm2 
sudo npm install -g npm@latest
sudo npm install pm2 -g
# Installation de mongodb et demarrage du service 
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 \
--recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse"\
| sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongod.service



############################################################################




# Configuation Nginx
cd  /etc/nginx/sites-available/
rm -rf default
sudo wget https://gitlab.com/yucf/deployment-script/-/raw/main/web.site.fr
sudo ln -s /etc/nginx/sites-available/web.site.fr /etc/nginx/sites-enabled/web.site.fr
sudo nginx -t
sudo systemctl restart nginx


# Cloner le projet "GITLAB"
cd /home/websrv
git clone -b prod https://gitlab.com/yucf/gestion-de-crise-insa.git
cd /home/websrv/gestion-de-crise-insa
npm i 
pm2 start index.js
